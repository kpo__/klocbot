
import flask

#!/usr/bin/python3
import RPi.GPIO as GPIO
import time

index = '<h1><a href="http://192.168.100.27:5000/api/forward"> FORWARD </a></br></br></br></br><a href="http://192.168.100.27:5000/api/left"> LEFT </a></br></br></br></br><a href="http://192.168.100.27:5000/api/right"> RIGHT </a></h1>'

class ServoMotor:
    def __init__(self, gpio, mid_pos = 7.1):
        self._crap_factor_forward = 1
        self._crap_factor_backward = 1
        self._current_speed = 0
        self._current_direction = "+"
        self._servo_pin = gpio
        self._min_pos = mid_pos - 5
        self._mid_pos = mid_pos
        self._max_pos = mid_pos + 5

        GPIO.setmode(GPIO.BCM)
        GPIO.setup(gpio, GPIO.OUT)
        self._servo = GPIO.PWM(gpio, 50) # 50 because 50hz
        self._servo.start(self._mid_pos)
        
    def _calculate_speed_to_signal_width(self, speed):
        new_max = self._max_pos
        new_min = self._min_pos
        
        old_value = speed
        old_max = 100
        old_min = -100
        
        old_range = (old_max - old_min)
        new_range = (new_max - new_min)
        new_value = (((old_value - old_min) * new_range) / old_range) + new_min

        return new_value

    def _set_servo_position(self, position):
        return self._servo.ChangeDutyCycle(position)
        

    def set_speed(self, speed):
        return self._set_servo_position(self._calculate_speed_to_signal_width(speed))

    def halt(self):
        self._servo.stop()

class KlocBot:
    def __init__(self, motor_left, motor_right):
        self._motor_left = motor_left
        self._motor_right = motor_right

    def set_speed(self, speed_left, speed_right):
        self._motor_left.set_speed(speed_left)
        self._motor_right.set_speed(-speed_right)


app = flask.Flask(__name__)
app.config["DEBUG"] = True

@app.route('/api/forward', methods=['GET'])
def forward():
    motor_left = ServoMotor(17)
    motor_right = ServoMotor(18)

    robot = KlocBot(motor_left, motor_right)
    robot.set_speed(100,100)
    time.sleep(1)
    robot.set_speed(0,0)

    return index

@app.route('/api/left', methods=['GET'])
def left():
    motor_left = ServoMotor(17)
    motor_right = ServoMotor(18)

    robot = KlocBot(motor_left, motor_right)
    robot.set_speed(-100,100)
    time.sleep(0.1)
    robot.set_speed(0,0)

    return index

@app.route('/api/right', methods=['GET'])
def right():
    motor_left = ServoMotor(17)
    motor_right = ServoMotor(18)

    robot = KlocBot(motor_left, motor_right)
    robot.set_speed(100,-100)
    time.sleep(0.1)
    robot.set_speed(0,0)

    return index

@app.route('/api/nod', methods=['GET'])
def nod():
    motor_left = ServoMotor(17)
    motor_right = ServoMotor(18)

    robot = KlocBot(motor_left, motor_right)
    robot.set_speed(-100,-100)
    time.sleep(0.4)
    robot.set_speed(100,100)
    time.sleep(0.2)

    return index

app.run(host = '0.0.0.0')
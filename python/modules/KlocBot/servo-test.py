#!/usr/bin/python3
import RPi.GPIO as GPIO
import time

servo_l_pin = 18
servo_r_pin = 17

GPIO.setmode(GPIO.BCM)
GPIO.setup(servo_l_pin, GPIO.OUT)
GPIO.setup(servo_r_pin, GPIO.OUT)

motor_l = GPIO.PWM(servo_l_pin, 50)
motor_r = GPIO.PWM(servo_r_pin, 50)

motor_l.start(2.5) # Initialization
motor_r.start(2.5)
try:
  while True:
    motor_l.ChangeDutyCycle(5)
    motor_r.ChangeDutyCycle(5)
    time.sleep(0.5)
    motor_l.ChangeDutyCycle(7.5)
    motor_r.ChangeDutyCycle(5)
    time.sleep(0.5)
    motor_l.ChangeDutyCycle(10)
    motor_r.ChangeDutyCycle(10)
    time.sleep(0.5)
    motor_l.ChangeDutyCycle(12.5)
    motor_r.ChangeDutyCycle(12.5)
    time.sleep(0.5)
    motor_l.ChangeDutyCycle(10)
    motor_r.ChangeDutyCycle(5)
    time.sleep(0.5)
    motor_l.ChangeDutyCycle(7.5)
    motor_r.ChangeDutyCycle(7.5)
    time.sleep(0.5)
    motor_l.ChangeDutyCycle(5)
    motor_r.ChangeDutyCycle(5)
    time.sleep(0.5)
    motor_l.ChangeDutyCycle(2.5)
    motor_r.ChangeDutyCycle(2.5)
    time.sleep(0.5)
except KeyboardInterrupt:
  motor_l.stop()
  motor_r.stop()
  GPIO.cleanup()
